# Indonesian translation for lomiri-indicator-network
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-indicator-network package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-network\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-25 18:01+0000\n"
"PO-Revision-Date: 2023-03-02 15:36+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Indonesian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"indicator-network/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.16.2-dev\n"
"X-Launchpad-Export-Date: 2016-10-21 07:05+0000\n"

#: doc/qt/qml/examples/example_networking_status.qml:46
msgid "Networking Status"
msgstr ""

#: src/agent/SecretRequest.cpp:61
#, qt-format
msgid "Connect to “%1”"
msgstr "Menyambung ke \"%1\""

#: src/agent/SecretRequest.cpp:66
msgid "WPA"
msgstr "WPA"

#: src/agent/SecretRequest.cpp:68
msgid "WEP"
msgstr "WEP"

#: src/agent/SecretRequest.cpp:74
msgid "Connect"
msgstr "Sambung"

#: src/agent/SecretRequest.cpp:75 src/vpn-editor/DialogFile/DialogFile.qml:156
msgid "Cancel"
msgstr "Batal"

#: src/indicator/factory.cpp:187
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/indicator/factory.cpp:198
msgid "Flight Mode"
msgstr "Mode Penerbangan"

#: src/indicator/factory.cpp:208
msgid "Cellular data"
msgstr "Data Seluler"

#: src/indicator/factory.cpp:221
msgid "Hotspot"
msgstr "Hotspot"

#: src/indicator/menuitems/wifi-link-item.cpp:109
msgid "Other network…"
msgstr "Jaringan lain…"

#: src/indicator/menuitems/wwan-link-item.cpp:90
msgid "No SIM"
msgstr "Tidak ada SIM"

#: src/indicator/menuitems/wwan-link-item.cpp:97
msgid "SIM Error"
msgstr "Galat SIM"

#: src/indicator/menuitems/wwan-link-item.cpp:105
msgid "SIM Locked"
msgstr "SIM Terkunci"

#: src/indicator/menuitems/wwan-link-item.cpp:118
msgid "Unregistered"
msgstr "Tak terdaftar"

#: src/indicator/menuitems/wwan-link-item.cpp:123
#: src/indicator/nmofono/hotspot-manager.cpp:111
msgid "Unknown"
msgstr "Tidak diketahui"

#: src/indicator/menuitems/wwan-link-item.cpp:128
msgid "Denied"
msgstr "Ditolak"

#: src/indicator/menuitems/wwan-link-item.cpp:133
msgid "Searching"
msgstr "Sedang mencari"

#: src/indicator/menuitems/wwan-link-item.cpp:145
msgid "No Signal"
msgstr "Tidak ada Sinyal"

#: src/indicator/menuitems/wwan-link-item.cpp:157
#: src/indicator/menuitems/wwan-link-item.cpp:164
msgid "Offline"
msgstr "Luring"

#: src/indicator/nmofono/hotspot-manager.cpp:51
msgid "Unknown error"
msgstr "Galat tak dikenal"

#: src/indicator/nmofono/hotspot-manager.cpp:53
msgid "No reason given"
msgstr "Tak ada alasan yang diberikan"

#: src/indicator/nmofono/hotspot-manager.cpp:55
msgid "Device is now managed"
msgstr "Perangkat kini dikelola"

#: src/indicator/nmofono/hotspot-manager.cpp:57
msgid "Device is now unmanaged"
msgstr "Perangkat kini tak dikelola"

#: src/indicator/nmofono/hotspot-manager.cpp:59
msgid "The device could not be readied for configuration"
msgstr "Perangkat tak dapat dibuat bersiap untuk ditata"

#: src/indicator/nmofono/hotspot-manager.cpp:61
msgid ""
"IP configuration could not be reserved (no available address, timeout, etc.)"
msgstr ""
"Konfigurasi IP tak bisa dicadangkan (tak ada alamat tersedia, habis waktu, "
"dsb.)"

#: src/indicator/nmofono/hotspot-manager.cpp:63
msgid "The IP configuration is no longer valid"
msgstr "Konfigurasi IP tak valid lagi"

#: src/indicator/nmofono/hotspot-manager.cpp:65
msgid "Your authentication details were incorrect"
msgstr "Rincian otentikasi Anda salah"

#: src/indicator/nmofono/hotspot-manager.cpp:67
msgid "802.1X supplicant disconnected"
msgstr "802.1X supplicant diputus"

#: src/indicator/nmofono/hotspot-manager.cpp:69
msgid "802.1X supplicant configuration failed"
msgstr "Konfigurasi 802.1X supplicant gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:71
msgid "802.1X supplicant failed"
msgstr "802.1X supplicant gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:73
msgid "802.1X supplicant took too long to authenticate"
msgstr "802.1X supplicant makan waktu terlalu lama untuk mengotentikasi"

#: src/indicator/nmofono/hotspot-manager.cpp:75
msgid "DHCP client failed to start"
msgstr "Klien DHCP gagal dimulai"

#: src/indicator/nmofono/hotspot-manager.cpp:77
msgid "DHCP client error"
msgstr "Galat klien DHCP"

#: src/indicator/nmofono/hotspot-manager.cpp:79
msgid "DHCP client failed"
msgstr "Klien DHCP gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:81
msgid "Shared connection service failed to start"
msgstr "Layanan koneksi bersama gagal dimulai"

#: src/indicator/nmofono/hotspot-manager.cpp:83
msgid "Shared connection service failed"
msgstr "Layanan koneksi bersama gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:85
msgid "Necessary firmware for the device may be missing"
msgstr "Firmware yang diperlukan bagi perangkat mungkin kurang"

#: src/indicator/nmofono/hotspot-manager.cpp:87
msgid "The device was removed"
msgstr "Perangkat dicabut"

#: src/indicator/nmofono/hotspot-manager.cpp:89
msgid "NetworkManager went to sleep"
msgstr "NetworkManager tidur"

#: src/indicator/nmofono/hotspot-manager.cpp:91
msgid "The device's active connection disappeared"
msgstr "Koneksi aktif perangkat menghilang"

#: src/indicator/nmofono/hotspot-manager.cpp:93
msgid "Device disconnected by user or client"
msgstr "Perangkat diputus oleh pengguna atau klien"

#: src/indicator/nmofono/hotspot-manager.cpp:95
msgid "The device's existing connection was assumed"
msgstr "Koneksi perangkat kini diasumsikan"

#: src/indicator/nmofono/hotspot-manager.cpp:97
msgid "The supplicant is now available"
msgstr "Supplicant kini tersedia"

#: src/indicator/nmofono/hotspot-manager.cpp:99
msgid "The modem could not be found"
msgstr "Modem tak dapat ditemukan"

#: src/indicator/nmofono/hotspot-manager.cpp:101
msgid "The Bluetooth connection failed or timed out"
msgstr "Koneksi Bluetooth gagal atau kehabisan waktu"

#: src/indicator/nmofono/hotspot-manager.cpp:103
msgid "A dependency of the connection failed"
msgstr "Suatu kebergantungan koneksi gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:105
msgid "ModemManager is unavailable"
msgstr "ModemManager tak tersedia"

#: src/indicator/nmofono/hotspot-manager.cpp:107
msgid "The Wi-Fi network could not be found"
msgstr "Jaringan Wi-Fi tak bisa ditemukan"

#: src/indicator/nmofono/hotspot-manager.cpp:109
msgid "A secondary connection of the base connection failed"
msgstr "Koneksi kedua dari koneksi dasar gagal"

#: src/indicator/nmofono/hotspot-manager.cpp:654
msgid "Failed to enable hotspot"
msgstr ""

#: src/indicator/nmofono/vpn/vpn-manager.cpp:87
#, qt-format
msgid "VPN connection %1"
msgstr "Koneksi VPN %1"

#. TRANSLATORS: this is the indicator title shown on the top header of the indicator area
#: src/indicator/root-state.cpp:314
msgid "Network"
msgstr "Jaringan"

#: src/indicator/sections/vpn-section.cpp:147
msgid "VPN settings…"
msgstr "Pengaturan VPN…"

#: src/indicator/sections/wifi-section.cpp:58
msgid "Wi-Fi settings…"
msgstr "Pengaturan Wi-Fi…"

#: src/indicator/sections/wwan-section.cpp:102
msgid "Cellular settings…"
msgstr "Pengaturan seluler…"

#: src/indicator/sim-unlock-dialog.cpp:144
msgid "Sorry, incorrect %{1} PIN."
msgstr "Maaf, PIN %{1} salah."

#: src/indicator/sim-unlock-dialog.cpp:149
#: src/indicator/sim-unlock-dialog.cpp:178
msgid "This will be your last attempt."
msgstr "Ini akan menjadi upaya terakhir Anda."

#: src/indicator/sim-unlock-dialog.cpp:151
msgid ""
"If %{1} PIN is entered incorrectly you will require your PUK code to unlock."
msgstr ""
"Bila PIN %{1} salah dimasukkan Anda akan memerlukan kode PUK untuk membuka "
"kunci."

#: src/indicator/sim-unlock-dialog.cpp:161
msgid "Sorry, your %{1} is now blocked."
msgstr "Maaf, %{1} Anda kini terblokir."

#: src/indicator/sim-unlock-dialog.cpp:166
msgid "Please enter your PUK code to unblock SIM card."
msgstr "Harap masukkan kode PUK Anda untuk membuka blokir kartu SIM."

#: src/indicator/sim-unlock-dialog.cpp:168
msgid "You may need to contact your network provider for PUK code."
msgstr "Anda mungkin perlu menghubungi operator seluler Anda untuk kode PUK."

#: src/indicator/sim-unlock-dialog.cpp:176
#: src/indicator/sim-unlock-dialog.cpp:190
msgid "Sorry, incorrect PUK."
msgstr "Maaf, PUK salah."

#: src/indicator/sim-unlock-dialog.cpp:180
msgid ""
"If PUK code is entered incorrectly, your SIM card will be blocked and needs "
"replacement."
msgstr ""
"Bila kode PUK salah dimasukkan, kartu SIM Anda akan terblokir dan perlu "
"penggantian."

#: src/indicator/sim-unlock-dialog.cpp:182
msgid "Please contact your network provider."
msgstr "Harap hubungi operator seluler Anda."

#: src/indicator/sim-unlock-dialog.cpp:192
msgid "Your SIM card is now permanently blocked and needs replacement."
msgstr "Kartu SIM Anda kini terblokir permanen dan perlu penggantian."

#: src/indicator/sim-unlock-dialog.cpp:194
msgid "Please contact your service provider."
msgstr "Harap hubungi operator seluler Anda."

#: src/indicator/sim-unlock-dialog.cpp:218
msgid "Sorry, incorrect PIN"
msgstr "Maaf, PIN salah"

#: src/indicator/sim-unlock-dialog.cpp:230
msgid "Sorry, incorrect PUK"
msgstr "Maaf, PUK salah"

#: src/indicator/sim-unlock-dialog.cpp:292
msgid "Enter %{1} PIN"
msgstr "Masukkan PIN %{1}"

#: src/indicator/sim-unlock-dialog.cpp:300
msgid "Enter PUK code"
msgstr "Masukkan kode PUK"

#: src/indicator/sim-unlock-dialog.cpp:304
msgid "Enter PUK code for %{1}"
msgstr "Masukkan kode PUK bagi %{1}"

#: src/indicator/sim-unlock-dialog.cpp:315
#, c-format
msgid "1 attempt remaining"
msgid_plural "%d attempts remaining"
msgstr[0] "%d percobaan tersisa"

#: src/indicator/sim-unlock-dialog.cpp:326
msgid "Enter new %{1} PIN"
msgstr "Masukkan PIN baru %{1}"

#: src/indicator/sim-unlock-dialog.cpp:334
msgid "Confirm new %{1} PIN"
msgstr "Konfirmasikan PIN baru %{1}"

#: src/indicator/sim-unlock-dialog.cpp:373
msgid "PIN codes did not match."
msgstr "Kode PIN tak cocok."

#: src/indicator/vpn-status-notifier.cpp:48
#, qt-format
msgid "The VPN connection '%1' failed."
msgstr "Koneksi VPN '%1' gagal."

#: src/indicator/vpn-status-notifier.cpp:50
#, qt-format
msgid ""
"The VPN connection '%1' failed because the network connection was "
"interrupted."
msgstr "Koneksi VPN '%1' gagal karena koneksi jaringan terputus."

#: src/indicator/vpn-status-notifier.cpp:51
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service stopped unexpectedly."
msgstr "Koneksi VPN '%1' gagal karena layanan VPN tiba-tiba berhenti."

#: src/indicator/vpn-status-notifier.cpp:52
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service returned invalid "
"configuration."
msgstr ""
"Koneksi VPN '%1' gagal karena layanan VPN mengembalikan konfigurasi yang tak "
"valid."

#: src/indicator/vpn-status-notifier.cpp:53
#, qt-format
msgid ""
"The VPN connection '%1' failed because the connection attempt timed out."
msgstr "Koneksi VPN '%1' gagal karena upaya koneksi habis waktu."

#: src/indicator/vpn-status-notifier.cpp:54
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service did not start in time."
msgstr "Koneksi VPN '%1' gagal karena layanan VPN tak dimulai pada waktunya."

#: src/indicator/vpn-status-notifier.cpp:55
#, qt-format
msgid "The VPN connection '%1' failed because the VPN service failed to start."
msgstr "Koneksi VPN '%1' gagal karena layanan VPN gagal mulai."

#: src/indicator/vpn-status-notifier.cpp:56
#, qt-format
msgid "The VPN connection '%1' failed because there were no valid VPN secrets."
msgstr "Koneksi VPN '%1' gagal karena tidak ada rahasia VPN yang valid."

#: src/indicator/vpn-status-notifier.cpp:57
#, qt-format
msgid "The VPN connection '%1' failed because of invalid VPN secrets."
msgstr "Koneksi VPN '%1' gagal karena rahasia VPN tak valid."

#: src/indicator/vpn-status-notifier.cpp:68
msgid "VPN Connection Failed"
msgstr "Sambungan VPN Gagal"

#: src/vpn-editor/DialogFile/DialogFile.qml:164
msgid "Accept"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:113 src/vpn-editor/Pptp/Advanced.qml:24
#: src/vpn-editor/Pptp/Editor.qml:92
msgid "Advanced"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:44
#: src/vpn-editor/Pptp/Advanced.qml:44
msgid "Only use connection for VPN resources"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:48
msgid "Use custom gateway port:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:64
msgid "Use renegotiation interval:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:85
msgid "Use LZO data compression"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:94
#, fuzzy
#| msgid "VPN connection %1"
msgid "Use a TCP connection"
msgstr "Koneksi VPN %1"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:98
msgid "Use custom virtual device type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:105
msgid "TUN"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:106
msgid "TAP"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:117
msgid "(automatic)"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:121
msgid "and name:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:125
msgid "Use custom tunnel MTU:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:142
msgid "Use custom UDP fragment size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:164
msgid "Restrict tunnel TCP MSS"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:173
msgid "Randomize remote hosts"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:28
#: src/vpn-editor/Openvpn/Editor.qml:132
msgid "Proxies"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:43
msgid "Proxy type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:45
msgid "Not required"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:46
msgid "HTTP"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:47
msgid "SOCKS"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:54
msgid "Server address:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:65
msgid "Port:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:77
msgid "Retry indefinitely:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:87
msgid "Proxy username:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:98
msgid "Proxy password:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:119 src/vpn-editor/Pptp/Advanced.qml:97
msgid "Security"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:39
msgid "Cipher:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:41
#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:83
msgid "Default"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:42
msgid "DES-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:43
msgid "RC2-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:44
msgid "DES-EDE-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:45
msgid "DES-EDE3-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:46
msgid "DESX-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:47
msgid "RC2-40-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:48
msgid "CAST5-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:49
msgid "AES-128-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:50
msgid "AES-192-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:51
msgid "AES-256-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:52
msgid "CAMELLIA-128-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:53
msgid "CAMELLIA-192-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:54
msgid "CAMELLIA-256-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:55
msgid "SEED-CBC"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:56
msgid "AES-128-CBC-HMAC-SHA1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:57
msgid "AES-256-CBC-HMAC-SHA1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:64
msgid "Use cipher key size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:81
msgid "HMAC authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:84
#: src/vpn-editor/Openvpn/AdvancedTls.qml:86
#: src/vpn-editor/Openvpn/StaticKey.qml:37
msgid "None"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:85
msgid "RSA MD-4"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:86
msgid "MD-5"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:87
msgid "SHA-1"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:88
msgid "SHA-224"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:89
msgid "SHA-256"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:90
msgid "SHA-384"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:91
msgid "SHA-512"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:92
msgid "RIPEMD-160"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:25
msgid "TLS authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:46
msgid "Subject match:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:50
msgid "Verify peer certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:57
msgid "Peer certificate TLS type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:59
msgid "Server"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:60
msgid "Client"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:68
msgid "Use additional TLS authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:75
msgid "Key file:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:84
#: src/vpn-editor/Openvpn/StaticKey.qml:35
msgid "Key direction:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:87
#: src/vpn-editor/Openvpn/StaticKey.qml:38
msgid "0"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:88
#: src/vpn-editor/Openvpn/StaticKey.qml:39
msgid "1"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:65 src/vpn-editor/Pptp/Editor.qml:35
msgid "General"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:74 src/vpn-editor/Pptp/Editor.qml:44
msgid "ID:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:77
msgid "Authentication"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:86
msgid "Remote:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:91
msgid "Certificates (TLS)"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:92
msgid "Password"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:93
msgid "Password with certificates (TLS)"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:94
msgid "Static key"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:98
msgid "Type:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:125
msgid "TLS"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:32
#: src/vpn-editor/Openvpn/PasswordTls.qml:32
msgid "Username:"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:42
#: src/vpn-editor/Openvpn/PasswordTls.qml:42 src/vpn-editor/Pptp/Editor.qml:76
msgid "Password:"
msgstr ""

#: src/vpn-editor/Openvpn/Password.qml:51
#: src/vpn-editor/Openvpn/PasswordTls.qml:60 src/vpn-editor/Openvpn/Tls.qml:40
msgid "CA certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:51 src/vpn-editor/Openvpn/Tls.qml:31
msgid "User certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:69 src/vpn-editor/Openvpn/Tls.qml:49
msgid "Private key:"
msgstr ""

#: src/vpn-editor/Openvpn/PasswordTls.qml:79 src/vpn-editor/Openvpn/Tls.qml:58
msgid "Key password:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:31
msgid "Static key:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:52
msgid "Remote IP:"
msgstr ""

#: src/vpn-editor/Openvpn/StaticKey.qml:62
msgid "Local IP:"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:47
msgid "Authentication methods"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:55
msgid "PAP"
msgstr "PAP"

#: src/vpn-editor/Pptp/Advanced.qml:65
msgid "CHAP"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:75
msgid "MSCHAP"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:84
msgid "MSCHAPv2"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:93
msgid "EAP"
msgstr "EAP"

#: src/vpn-editor/Pptp/Advanced.qml:105
msgid "Use Point-to-Point encryption"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:110
msgid "All available (default)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:111
msgid "128-bit (most secure)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:112
msgid "40-bit (less secure)"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:125
msgid "Allow stateful encryption"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:128
msgid "Compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:136
msgid "Allow BSD data compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:145
msgid "Allow Deflate data compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:154
msgid "Use TCP header compression"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:157
msgid "Echo"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:165
msgid "Send PPP echo packets"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:54
msgid "Gateway:"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:57
msgid "Optional"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:66
msgid "User name:"
msgstr ""

#: src/vpn-editor/Pptp/Editor.qml:86
msgid "NT Domain:"
msgstr ""

#: src/vpn-editor/VpnEditor.qml:25
#, qt-format
msgid "Editing: %1"
msgstr ""

#: src/vpn-editor/VpnList.qml:26
#, fuzzy
#| msgid "VPN connection %1"
msgid "VPN configurations"
msgstr "Koneksi VPN %1"

#: src/vpn-editor/VpnList.qml:40
msgid "OpenVPN"
msgstr ""

#: src/vpn-editor/VpnList.qml:45
msgid "PPTP"
msgstr ""

#: src/vpn-editor/VpnList.qml:86
msgid "Delete configuration"
msgstr ""

#: src/vpn-editor/VpnList.qml:100
#, fuzzy
#| msgid "VPN connection %1"
msgid "No VPN connections"
msgstr "Koneksi VPN %1"
