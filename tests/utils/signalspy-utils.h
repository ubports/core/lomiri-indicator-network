/*
 * Copyright (C) 2015 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Pete Woods <pete.woods@canonical.com>
 */

#pragma once

#include <vector>
#include <utility>

#include <QList>
#include <QVariant>

#include <gtest/gtest.h>

#define WAIT_FOR_SIGNALS(signalSpy, signalsExpected)\
{\
	while (signalSpy.size() < signalsExpected)\
	{\
		ASSERT_TRUE(signalSpy.wait()) << "Waiting for " << signalsExpected << " signals, got " << signalSpy.size();\
	}\
	ASSERT_EQ(signalsExpected, signalSpy.size()) << "Waiting for " << signalsExpected << " signals, got " << signalSpy.size();\
}

#define WAIT_FOR_ROW_COUNT(signalSpy, model, expectedRowCount)\
{\
	while (model->rowCount() < expectedRowCount)\
	{\
		ASSERT_TRUE(signalSpy.wait()) << "Waiting for model to have " << expectedRowCount << " rows, got " << model->rowCount();\
	}\
	ASSERT_EQ(expectedRowCount, model->rowCount()) << "Waiting for model to have " << expectedRowCount << " rows, got " << model->rowCount();\
}

#define CHECK_METHOD_CALL(signalSpy, signalIndex, methodName, ...)\
{\
	QList<QVariant> const& call(signalSpy.at(signalIndex));\
	EXPECT_EQ(methodName, call.at(0));\
	auto arguments = std::vector<std::pair<int, QVariant>>{__VA_ARGS__};\
	if (!arguments.empty())\
	{\
		QList<QVariant> const& args(call.at(1).toList());\
		ASSERT_LE(arguments.back().first + 1, args.size());\
		for (auto const& argument : arguments)\
		{\
			EXPECT_EQ(argument.second, args.at(argument.first));\
		}\
	}\
}

