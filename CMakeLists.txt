cmake_minimum_required(VERSION 3.13)

project(lomiri-indicator-network VERSION 1.1.0 LANGUAGES C CXX)

string(TOLOWER "${CMAKE_BUILD_TYPE}" build_type_lower)

if("${build_type_lower}" STREQUAL debug)
  set(TRACE_DEFAULT TRUE)
else()
  set(TRACE_DEFAULT FALSE)
endif()

option(trace_messages "Print debug trace messages." ${TRACE_DEFAULT})
option(REMOTE_BUILD "Remote build (skip docs, translations, tests)." FALSE)
option(ENABLE_TESTS "Enable tests" TRUE)
option(ENABLE_COVERAGE "Generate Coverage Reports" TRUE)
option(ENABLE_MECHANICD "Enable integration with Mechanicd" OFF)
option(ENABLE_UBUNTU_COMPAT "Enable Ubuntu.Connectivity QML compatibility layer" OFF)
option(USE_SYSTEMD "Install systemd services" ON)

if(${trace_messages})
  add_definitions(-DINDICATOR_NETWORK_TRACE_MESSAGES)
endif()

if(ENABLE_MECHANICD)
  add_definitions(-DENABLE_MECHANICD)
endif()

add_definitions(
  -DQT_NO_KEYWORDS=1
)

find_package(PkgConfig REQUIRED)
include(GNUInstallDirs)
find_package(GSettings)

set(PROJECT_VERSION "16.10.0")
set(PACKAGE ${CMAKE_PROJECT_NAME})
set(GETTEXT_PACKAGE lomiri-indicator-network)
add_definitions (
    -DGETTEXT_PACKAGE="${GETTEXT_PACKAGE}"
    -DLOCALE_DIR="${CMAKE_INSTALL_FULL_LOCALEDIR}"
)

set(DATA_DIR "${CMAKE_SOURCE_DIR}/data")

set(GLIB_REQUIRED_VERSION 2.26)

pkg_check_modules(
  GLIB REQUIRED
  glib-2.0>=${GLIB_REQUIRED_VERSION}
  gio-2.0>=${GLIB_REQUIRED_VERSION}
)
include_directories(${GLIB_INCLUDE_DIRS})

pkg_check_modules(
  LOMIRI_API REQUIRED
  liblomiri-api
)
include_directories(${LOMIRI_API_INCLUDE_DIRS})

set(OFONO_REQUIRED_VERSION 1.12)
pkg_check_modules(
  OFONO REQUIRED
  ofono>=${OFONO_REQUIRED_VERSION}
)
include_directories(${OFONO_INCLUDE_DIRS})

pkg_check_modules(
  LIBSECRET REQUIRED
  libsecret-1
)
include_directories(${LIBSECRET_INCLUDE_DIRS})

find_package(Qt5Core REQUIRED)
include_directories(${Qt5Core_INCLUDE_DIRS})

find_package(Qt5DBus COMPONENTS Qt5DBusMacros REQUIRED)
include_directories(${Qt5DBus_INCLUDE_DIRS})

pkg_check_modules(NM REQUIRED libnm REQUIRED)
include_directories(${NM_INCLUDE_DIRS})

pkg_check_modules(URL_DISPATCHER REQUIRED lomiri-url-dispatcher)
include_directories(${URL_DISPATCHER_INCLUDE_DIRS})

if(USE_SYSTEMD)
  pkg_check_modules(SYSTEMD systemd REQUIRED)
endif()

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include_directories(${CMAKE_BINARY_DIR})

set(COMMON_FLAGS "-Wall -Wextra -Wpedantic -fno-permissive -fPIC -fvisibility=hidden -pthread")

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 14)
if(CMAKE_COMPILER_IS_GNUCC)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMMON_FLAGS}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_FLAGS}")
endif()

set(CONNECTIVITY_QT_VERSION_MAJOR 1)
set(CONNECTIVITY_QT_VERSION_MINOR 0)
set(CONNECTIVITY_QT_LIB_TARGET lomiri-connectivity-qt${CONNECTIVITY_QT_VERSION_MAJOR})

add_subdirectory(data)
add_subdirectory(src)

if(NOT REMOTE_BUILD)

add_subdirectory(po)
add_subdirectory(doc)

if(ENABLE_TESTS)
enable_testing()
add_subdirectory(tests)

ADD_CUSTOM_TARGET(
        check
        ${CMAKE_CTEST_COMMAND} --force-new-ctest-process --output-on-failure
)

if(ENABLE_COVERAGE)
  find_package(CoverageReport)
  enable_coverage_report(
    TARGETS
      lomiri-indicator-network-service
      menumodel_cpp
      notify_cpp
      qdbus-stubs
      i-n-sniffer
      url_dispatcher_cpp
    TESTS
      unit-tests
      integration-tests
      secret-agent-test-bin
    FILTER
      ${CMAKE_SOURCE_DIR}/tests/*
      ${CMAKE_BINARY_DIR}/*
  )
endif()

endif()

endif()

add_subdirectory(scripts)
