
add_subdirectory(connectivity-api)

add_subdirectory(agent)
add_subdirectory(indicator)
add_subdirectory(menumodel-cpp)
add_subdirectory(qdbus-stubs)
add_subdirectory(qrepowerd)
add_subdirectory(notify-cpp)
add_subdirectory(url-dispatcher-cpp)
add_subdirectory(util)
